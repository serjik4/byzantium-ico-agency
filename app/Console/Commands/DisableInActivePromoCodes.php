<?php

namespace App\Console\Commands;

use App\PromoCode;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Library\Api\GetResponse;

class DisableInActivePromoCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promocode:disable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable promo codes for which activation time has been expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $promocodes = PromoCode::where([
            ['expirationDate', '<', Carbon::now()->format('Y-m-d h:m:s')],
            ['status', '<>', '-1']
        ])->get()->all();

        foreach ($promocodes as $promocode)
        {
            if(!$promocode->isPay())
            {
                $promocode->status = -1;
                $promocode->save();
                Log::info('Promocode' . $promocode->code . 'has been deactivated.');
            }
            if($promocode->getParticipantId())
            {
                $gr = new GetResponse('6395eda0a41ecf11c841f36989446ae3');
                $gr->deleteContact($promocode->getGetResponseId());
            }
        }
    }
}
//php C:\OSPanel\domains\bzntm.loc\artisan schedule:run