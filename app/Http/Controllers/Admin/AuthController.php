<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function getLogin()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ]))
        {
            return redirect()->route('participants.index');
        }
        return redirect()->back()->with('status', 'Неверный логин или пароль');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
