<?php

namespace App\Http\Controllers\Admin;

use App\Participant;
use App\PromoCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use App\Library\Api\GetResponse;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participants = Participant::all();
        return view('admin.participants.index', [
            'participants' => $participants,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $participant = Participant::find($id);
        return view('admin.participants.edit', [
            'participant' => $participant,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->validate($request, [
            'photo' => 'nullable|image|mimes:jpg,png,jpeg',
        ]);

        $promocode = PromoCode::checkPromoCode($request->get('promo_code'));
        $participant = Participant::find($id);

        if($promocode)
        {
            if($promocode->status == -1 && $participant->getParticipantPromoCode() != $promocode->code)
            {
                return redirect()->back()->with('status', 'Promotional code has expired');
            } elseif ($promocode->status == 0 && $participant->getParticipantPromoCode() != $promocode->code)
            {
                return redirect()->back()->with('status', 'Promotional code already in use');
            }


            if($participant->getParticipantPromoCode() && $participant->promocode->status != -1 && $participant->getParticipantPromoCode() != $promocode->code)
            {
                $participant->promocode->status = 1;
                $participant->promocode->participant_id = null;
                $participant->promocode->save();
                $participant->promo_code_id = $promocode->id;
                $participant->save();
            } else {
                $participant->promo_code_id = $promocode->id;
                $participant->save();
                if(!$participant->password)
                {
                    $password = $participant->setParticipantPassword($promocode->code);
                    $participant->sendEmailWithParticipantInfo($password, $participant->email);

                    /*add to "not_paid" company GetResponse*/
//                    $gr = new GetResponse('6395eda0a41ecf11c841f36989446ae3');
//
//                    $contact_data = [
//                        //'name' => $request->get('first_name') . $request->get('last_name'),
//                        'email' => $participant->email,
//                        'campaign' => [
//                            'campaignId' => 'nmFpU'//not_paid company getresponse
//                        ],
//                        'dayOfCycle' => '0',
//                    ];
//
//                    $gr->addContact($contact_data);
//
//                    $contact = $gr->getContacts([
//                        'query' => [
//                            'email' => $participant->email,
//                            'campaignId' => 'nmFpU'
//                        ]
//                    ]);
//                    $grcontact = (array)$contact;
//
//                    $participant->gr_id = $grcontact[0]->contactId;
//                    $participant->save();

                    /*nmFpU - not_paid , nmFVB - paid_for */
                }
            }

            $promocode->AssignPromoCode($participant->id);

        } elseif(!$promocode)
        {
            return redirect()->back()->with('status', 'Wrong promotional code');
        }

        $participant->edit(array_filter($request->all()));
        $toggle = $participant->toggleIsPay($request->get('is_pay'));
//        if($toggle)
//        {
//            //change company in getresponse
//            $gr = new GetResponse('6395eda0a41ecf11c841f36989446ae3');
//
//            $params = [
//                'campaign' => [
//                    'campaignId' => 'nmFVB'
//                ]
//            ];
//
//            $contact = $gr->updateContact($participant->gr_id, $params);
//            //dd($contact);
//            /*nmFpU - not_paid token, nmFVB - paid_for token*/
//        }
        $participant->uploadPhoto($request->file('photo'));

        return redirect()->route('participants.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
