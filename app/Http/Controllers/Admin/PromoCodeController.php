<?php

namespace App\Http\Controllers\Admin;

use App\PromoCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promocodes = PromoCode::all();
        return view('admin.promocode.index', [
            'promocodes' => $promocodes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promocode.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|regex:/^[A-Za-z0-9]+$/|min:5',
            'source' => 'required',
            'expirationDate' => 'required',
        ]);

        PromoCode::create($request->all());

        return redirect()->route('codes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PromoCode::find($id)->delete();
        return redirect()->route('codes.index');
    }

    public function generatePromoCodes(Request $request)
    {
        $this->validate($request, [
            'count' => 'required',
            'expirationDate' => 'required',
            'source' => 'required',
        ]);

        for ($i = 1; $i <= $request->get('count'); $i++)
        {
            $code = PromoCode::generateRandomPromocode(6);
            PromoCode::create([
                'code' => $code,
                'expirationDate' => $request->get('expirationDate'),
                'source' => $request->get('source'),
            ]);
        }

        return redirect()->route('codes.index')->with('status', 'Промокоды сгенерированы');
    }

    public function getDisabledPromoCodes()
    {
        $promocodes = PromoCode::where('status', '-1')->get();
        return view('admin.promocode.disable', [
            'promocodes' => $promocodes,
        ]);
    }

    public function xlsPromoCode()
    {
        $promocodes = PromoCode::all();
//        dd($promocodes);
        Excel::create('promo', function ($excel) use ($promocodes) {
            $excel->sheet('promo', function ($sheet) use ($promocodes) {
                $sheet->cell('A1', 'Source');
                for($i = 2; $i <= count($promocodes) + 1; $i++)
                {
                    $sheet->cell('A'.$i, $promocodes[$i - 2]->source);//
                }

                $sheet->cell('B1', 'Code');
                for($i = 2; $i <= count($promocodes) + 1; $i++)
                {
                    $sheet->cell('B'.$i, $promocodes[$i - 2]->code);//
                }

                $sheet->cell('C1', 'Email');
                for($i = 2; $i <= count($promocodes) + 1; $i++)
                {
                    $sheet->cell('C'.$i, $promocodes[$i - 2]->getParticipantEmail());//
                }

                $sheet->cell('D1', 'Name');
                for($i = 2; $i <= count($promocodes) + 1; $i++)
                {
                    $sheet->cell('D'.$i, $promocodes[$i - 2]->getParticipantFirstName());//
                }

                $sheet->cell('E1', 'ExpirationDate');
                for($i = 2; $i <= count($promocodes) + 1; $i++)
                {
                    $sheet->cell('E'.$i, $promocodes[$i - 2]->expirationDate);//
                }
            });
        })->download('xlsx');
    }
//    public function show()
//    {
//        return abort(404);
//    }
}
