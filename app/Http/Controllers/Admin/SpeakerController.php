<?php
//top 10 stage
//second stage
namespace App\Http\Controllers\Admin;

use App\Speaker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SpeakerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Speaker::all()->sortBy('time_start')->all());
        $speakers = Speaker::where('is_speaker', '=', 1)->get()->all();
        return view('admin.speakers.index', [
            'speakers' => $speakers,
        ]);
    }

    public function indexEvents()
    {
        $speakers = Speaker::where('is_speaker', '=', 0)->get()->all();
        return view('admin.events.index', [
            'speakers' => $speakers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.speakers.create');
    }

    public function createEvent()
    {
        return view('admin.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'title' => 'required',
            'photo' => 'nullable|image',
        ]);

        $speaker = Speaker::add($request->all());
        $speaker->uploadPhoto($request->file('photo'));
        $speaker->toggleSpeaker($request->get('is_speaker'));

        return redirect()->route('speakers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//        //
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $speaker = Speaker::find($id);
        return view('admin.speakers.edit', [
            'speaker' => $speaker,
        ]);
    }

    public function editEvent($id)
    {
        $speaker = Speaker::find($id);
        return view('admin.events.edit', [
            'speaker' => $speaker,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'photo' => 'nullable|image',
        ]);

        $speaker = Speaker::find($id);
        $speaker->edit($request->all());
        $speaker->uploadPhoto($request->file('photo'));

        return redirect()->route('speakers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Speaker::find($id)->delete();
        return redirect()->route('speakers.index');
    }
}
