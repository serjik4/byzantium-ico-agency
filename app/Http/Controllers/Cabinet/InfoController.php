<?php

namespace App\Http\Controllers\Cabinet;

use App\Participant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class InfoController extends Controller
{
    public function index()
    {
        if(Auth::guard('participant')->check())
        {
            $participant = Auth::guard('participant')->user();
            return view('cabinet.info', [
                'participant' => $participant,
            ]);
        }

        return abort(404);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'photo' => 'nullable|image|mimes:jpg,png,jpeg',
        ]);

        $participant = Auth::guard('participant')->user();
        $participant->edit(array_filter($request->all()));
        $participant->uploadPhoto($request->file('photo'));

        return redirect()->back()->with('status', 'Успешно обновили информацию');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'min:1'
        ]);
        if($request->get('password') != $request->get('repeat_password')){
            return redirect()->back()->with('status', 'Пароли не совпадают');
        }

        $participant = Auth::guard('participant')->user();
        $participant->updatePassword($request->get('password'));

        return redirect()->back()->with('status', 'Пароль успешно обновлен');
    }
}
