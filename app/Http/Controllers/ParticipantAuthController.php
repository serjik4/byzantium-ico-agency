<?php

namespace App\Http\Controllers;

use App\PromoCode;
use Illuminate\Http\Request;
use App\Participant;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Library\Api\GetResponse;

class ParticipantAuthController extends Controller
{
    public function getRegister()
    {
        return view('join');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'email' => 'required|email|unique:participants',
        ]);

        $promocode = PromoCode::where('code', $request->get('code'))->firstOrFail();
        $participant = Participant::add($request->all());
        $password = $participant->setParticipantPassword($request->get('code'));
        //dd($password);
        $participant->promo_code_id = $promocode->id;
        $participant->save();
        $promocode->disablePromoCode();
        $promocode->setActivationDate();
        $promocode->setParticipantId($participant->id);

        /*add to "not_paid" company GetResponse*/
//        $gr = new GetResponse('6395eda0a41ecf11c841f36989446ae3');
//
//        $contact_data = [
//            //'name' => $request->get('first_name') . $request->get('last_name'),
//            'email' => $request->get('email'),
//            'campaign' => [
//                'campaignId' => 'nmFpU'
//            ],
//            'dayOfCycle' => '0',
//        ];
//
//        $gr->addContact($contact_data);
//
//        $contact = $gr->getContacts([
//            'query' => [
//                'email' => $request->get('email')
//            ]
//        ]);
//        $grcontact = (array)$contact;
//
//        $participant->gr_id = $grcontact[0]->contactId;
//        $participant->save();

        /*nmFpU - not_paid token, nmFVB - paid_for token*/

        Auth::guard('participant')->attempt([
            'email' => $request->get('email'),
            'password' => $password,
        ]);

        $participant->sendEmailWithParticipantInfo($password, $request->get('email'));

        return redirect()->route('info.index')->with('status', 'Registration completed successfully');
    }

    public function getLogin()
    {
        return view('cabinet.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(Auth::guard('participant')->attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ]))
        {
            $user = Auth::guard('participant')->user();
            if(Carbon::parse($user->promocode->expirationDate)->timestamp <= Carbon::now()->timestamp && !$user->promocode->isPay())
            {
                Auth::guard('participant')->logout();
                return redirect()->back()->with('status', 'Your promocode has been diactivated. Please contact support');
            }
            return redirect()->route('info.index');

        }

        return redirect()->back()->with('status', 'Incorrect username or password');
    }

    public function logout()
    {
        Auth::guard('participant')->logout();
        return redirect('/');
    }

    public function getInvite()
    {
        return view('invite');
    }

    public function invite(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:participants',
        ]);

        $participant = Participant::add($request->all());

        $gr = new GetResponse('70652196724d3c23d7d7584cf0ebb7a4');

        $contact_data = [
            'name' => $request->get('first_name') . $request->get('last_name'),
            'email' => $request->get('email'),
            'campaign' => [
                'campaignId' => 'nmX9Q'
            ],
            'dayOfCycle' => '0',
        ];

        $grcontact = $gr->addContact($contact_data);


        return response()->json($grcontact);
    }

//    public function subscribe(Request $request)
//    {
//        Storage::append('list.csv', $request->get('email'));
//        return response()->json(true);
//    }

    public function becomeSponsor(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        Mail::send('email.become-sponsor', ['first_name' => $request->get('first_name'), 'last_name' => $request->get('last_name'), 'email' => $request->get('email'), 'company' => $request->get('company'), 'title_t' => $request->get('title_t'), 'message_t' => $request->get('message')], function ($m) {
            $m->from('top10@bzntm.com', 'TOP10 Point of Trust');
            $m->to('top10@bzntm.com')->subject('Become a sponsor');
        });

        return response()->json(true);
    }

    public function contactUs(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        Mail::send('email.contact-us', ['name' => $request->get('name'), 'email' => $request->get('email'), 'message_t' => $request->get('message')], function ($m) {
            $m->from('top10@bzntm.com', 'TOP10 Point of Trust');
            $m->to('top10@bzntm.com')->subject('Contact us');//emaimur@bzntm.com
        });

        return response()->json(true);
    }
}
