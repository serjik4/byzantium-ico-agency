<?php

namespace App\Http\Middleware;

use App\Participant;
use Closure;

class CheckParticipantPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $participant = Participant::where('email', $request->get('email'))->first();
        if($participant == null)
        {
            return redirect()->back()->with('status', 'Could not find the user with this email address');
        } else {
            $participantPromoCode = $participant->promo_code_id;
            if($participantPromoCode == null)
            {
                return redirect()->back()->with('status', 'Account is not active, please contact support');
            }
        }

        return $next($request);
    }
}
