<?php

namespace App\Http\Middleware;

use App\PromoCode;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CheckPromoCodesBeforeJoin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'code' => 'required'
        ]);

        $code = PromoCode::checkPromoCode($request->get('code'));

        if(!$code)
        {
            return redirect()->back()->with('status', 'Wrong promotional code');
        } elseif (Carbon::parse($code->expirationDate)->timestamp <= Carbon::now()->timestamp) {
            $code->status = -1;
            $code->save();
            return redirect()->back()->with('status', 'Promotional code has expired. Please, contact <a href="#" class="js-contact-us" style="color: #fff;text-decoration: underline;">Support.</a>');
        } elseif (!$code->status) {
            return redirect()->back()->with('status', 'This promocode code is already registered. Please, log in.');
        }

        return $next($request, $code);
    }
}
