<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ParticipantCabinetMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('participant')->check())
        {
            $user = Auth::guard('participant')->user();
            //dd($user);
            if(Carbon::parse($user->promocode->expirationDate)->timestamp <= Carbon::now()->timestamp && !$user->promocode->isPay())
            {
                Auth::guard('participant')->logout();
                return redirect()->route('participant.login')->with('status', 'Promotional code has expired, please contact support.');
            }

            return $next($request);
        }

        return abort(404);
    }
}
