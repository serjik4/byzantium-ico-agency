<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;

class Participant extends Authenticatable
{
    use Notifiable;

    protected $guarded = 'participants';

    protected $fillable = ['email', 'first_name', 'last_name', 'birthday', 'company', 'position', 'country', 'city'];

    protected $hidden = ['password', 'remember_token'];

    public function promocode()
    {
        return $this->hasOne(PromoCode::class);
    }

    public static function add($fields)
    {
        $participant = new Participant();
        $participant->fill($fields);
        $participant->save();

        return $participant;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function generatePassword()
    {
        $password = str_random(10);
        $this->password = bcrypt($password);
        $this->save();

        return $password;
    }

    public function updatePassword($password)
    {
        if($password != null)
        {
            $this->password = bcrypt($password);
            $this->save();
        }
    }

    public function getPhoto()
    {
        if($this->photo == null)
        {
            return 'img/no-image.png';
        }

        return 'uploads/participants/' . $this->photo;

    }

    public function uploadPhoto($image)
    {
        if($image == null){return;}
        if($this->photo != null)
        {
            unlink(public_path('uploads/participants/' . $this->photo));
        }
        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads/participants', $filename, 'public');
        $this->photo = $filename;
        $this->save();
    }

    public function getBirthdayAttribute($value)
    {
        if($value == null){return $value;}
        $date = Carbon::createFromFormat('Y-m-d h:m:s', $value)->format('d/m/y');
        return $date;
    }

    public function setBirthdayAttribute($value)
    {
        if($value == null){return $value;}
        $date = Carbon::createFromFormat('d/m/y', $value)->format('Y-m-d h:m:s');
        return $this->attributes['birthday'] = $date;
    }

    public function getParticipantPromoCode()
    {
        if($this->promocode != null)
        {
            return $this->promocode->code;
        }

        return false;
    }

    public function getExpirationDate()
    {
        if($this->promocode != null)
        {
            $date = Carbon::parse($this->promocode->expirationDate)->format('F d Y 00:00:01');
            return $date;
        }

        return false;
    }

    public function toggleIsPay($status)
    {
        if($status == null)
        {
            $this->is_pay = 0;
            $this->save();
            return $status;
        }
        $this->is_pay = 1;
        $this->save();
        return $status;
    }

    public function setParticipantPassword($code)
    {
        $this->password = bcrypt($code);
        $this->save();

        return $code;
    }

    /*---*/
    public function sendEmailWithParticipantInfo($password, $email)
    {
        Mail::send('email.password', ['password' => $password, 'email' => $email], function ($m) {
            $m->from('top10@bzntm.com', 'TOP10 Point of Trust');
            $m->to($this->email)->subject('Your password');
        });
    }
}
