<?php

namespace App;

use Faker\Provider\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Partner extends Model
{
    protected $fillable = ['title', 'description', 'facebook_link', 'linkedin_link'];

    public static function add($fields)
    {
        $partner = new Partner();
        $partner->fill($fields);
        $partner->save();

        return $partner;
    }

    public function uploadPhoto($image)
    {
        if($image == null){return;}

        if($this->photo != null)
        {
            unlink(public_path('uploads/partners/'. $this->photo));
        }
        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads/partners', $filename, 'public');//('folder', 'filename', 'disk')
        $this->photo = $filename;
        $this->save();
    }

    public function getPhoto()
    {
        if($this->photo == null)
        {
            return '/img/no-image.png';
        }

        return 'uploads/partners/' . $this->photo;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }
}
