<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;

class PromoCode extends Model
{
    protected $fillable = ['code', 'expirationDate', 'source'];

    public function participant()
    {
        return $this->hasOne(Participant::class);
    }

    public static function checkPromoCode($code)
    {
        if($code != null)
        {
            try
            {
                $promoCode = PromoCode::where('code', $code)->firstOrFail();
            } catch (ModelNotFoundException $Exception)
            {
                Log::info($Exception->getMessage());
                return false;
            }
            return $promoCode;
        }

        return null;
    }

    public function getParticipantEmail()
    {
        if($this->participant != null)
        {
            return $this->participant->email;
        }

        return 'empty';
    }

    public function getParticipantId()
    {
        if($this->participant != null)
        {
            return $this->participant->id;
        }

        return false;
    }

    public function getGetResponseId()
    {
        if($this->participant != null)
        {
            return $this->participant->gr_id;
        }

        return false;
    }

    public function disablePromoCode()
    {
        $this->status = 0;
        $this->save();
    }

    public function setActivationDate()
    {
        $this->activationDate = Carbon::now()->toDateTimeString();
        $this->save();
    }

    public function setParticipantId($id)
    {
        $this->participant_id = $id;
        $this->save();
    }

    public function getParticipantFirstName()
    {
        if($this->participant != null)
        {
            return $this->participant->first_name;
        }

        return 'empty';
    }

//    public function getExpirationDateAttribute($value)
//    {
//        //dd($value);
//        $date = Carbon::createFromFormat('Y-m-d h:m:s', $value)->format('d-m-y 00:00:01');
//        //dd($date);
//        return $date;
//    }

    public function setExpirationDateAttribute($value)
    {
        $date = Carbon::createFromFormat('d/m/y', $value)->format('Y-m-d 00:00:01');
        return $this->attributes['expirationDate'] = $date;
    }

    public static function generateRandomPromocode($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function AssignPromoCode($participant_id)
    {
        $this->participant_id = $participant_id;
        $this->disablePromoCode();
    }

    public function isPay()
    {
        if($this->participant != null)
        {
            if($this->participant->is_pay)
            {
                return true;
            }
            return false;
        }
        return false;
    }
}
