<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speaker extends Model
{
    protected $fillable = ['title', 'description', 'facebook_link', 'linkedin_link', 'company', 'time_start', 'time_end'];

    public static function add($fields)
    {
        $speaker = new Speaker();
        $speaker->fill($fields);
        $speaker->save();

        return $speaker;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function uploadPhoto($image)
    {
        if($image == null){return;}
        if($this->photo != null)
        {
            unlink(public_path('uploads/speakers/' . $this->photo));
        }
        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads/speakers', $filename, 'public');
        $this->photo = $filename;
        $this->save();
    }

    public function getPhoto()
    {
        if($this->photo == null)
        {
            return 'img/no-image.png';
        }

        return 'uploads/speakers/' . $this->photo;
    }

    public function setSpeaker()
    {
        $this->is_speaker = 1;
        $this->save();
    }

    public function setEvent()
    {
        $this->is_speaker = 0;
        $this->save();
    }

    public function toggleSpeaker($value)
    {
        if($value)
        {
            return $this->setSpeaker();
        }
        return $this->setEvent();
    }
}
