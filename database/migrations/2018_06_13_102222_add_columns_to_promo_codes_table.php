<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promo_codes', function (Blueprint $table){
            $table->string('source')->after('status');
            $table->timestamp('activationDate')->after('source')->nullable();
            $table->timestamp('expirationDate')->after('activationDate')->nullable();
            $table->string('participant_id')->after('expirationDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promo_codes', function (Blueprint $table){
            $table->dropColumn(['source', 'activationDate', 'expirationDate', 'participant_id']);
        });
    }
}
