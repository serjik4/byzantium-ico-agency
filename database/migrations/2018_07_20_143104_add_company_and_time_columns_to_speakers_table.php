<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyAndTimeColumnsToSpeakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('speakers', function (Blueprint $table) {
            $table->string('time_start')->after('photo')->nullable();
            $table->string('time_end')->after('time_start')->nullable();
            $table->string('company')->after('description')->nullable();
            $table->integer('is_speaker')->after('time_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('speakers', function (Blueprint $table) {
            $table->dropColumn(['time_start', 'time_end', 'company', 'is_speaker']);
        });
    }
}
