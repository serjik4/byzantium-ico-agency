jQuery(document).ready(function($) {
	
	$(document).on('click touch','.js-get-invite-link', function() {
		$('.get-invite-modal').toggleClass('open');
	});
	$(document).on('click touch','.js-become-sponsor-link', function() {
		$('.become-sponsor-modal').toggleClass('open');
	});
    $(document).on('click touch','.js-contact-us', function() {
        $('.contact-modal').toggleClass('open');
    });
	$(document).on('click touch','.js-close-modal', function() {
		$(this).closest('div').toggleClass('open');
	});

	$('#invite-form').on('submit', function (e) {
		e.preventDefault();

		var data = $(this).serialize();
		var url = '/invite';

        $.ajax({
			type: 'POST',
			data: data,
            url: url,
            cache: false,
            beforeSend: function () {

            },
            success: function (participant) {
				if(participant)
				{
					$('.get-invite-modal').removeClass('open');
					$('.thank-you-modal').toggleClass('open');
				}
            },
            error: function (jqXHR) {
				console.log(jqXHR);//if error

            }
		});
    });

    $('#sponsor-form').on('submit', function (e) {
        e.preventDefault();

        var data = $(this).serialize();
        var url = '/become-sponsor';

        //console.log(data);

        $.ajax({
            type: 'POST',
            data: data,
            url: url,
            cache: false,
            beforeSend: function () {

            },
            success: function (json) {
                if(json)
                {
                    $('.become-sponsor-modal').removeClass('open');
                    $('.thank-you-modal').toggleClass('open');
                }
            },
            error: function (jqXHR) {
                console.log(jqXHR);//if error

            }
        });
    });

    $('#contact-form').on('submit', function (e) {
        e.preventDefault();

        var data = $(this).serialize();
        var url = '/contact';

        //console.log(data);

        $.ajax({
            type: 'POST',
            data: data,
            url: url,
            cache: false,
            beforeSend: function () {

            },
            success: function (json) {
                if(json)
                {
                    $('.contact-modal').removeClass('open');
                    $('.thank-you-modal').toggleClass('open');
                }
            },
            error: function (jqXHR) {
                console.log(jqXHR);//if error

            }
        });
    });


///////////////////////////////////////////

function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(endtime) {
  // var clock = $("#"+id);
  var daysSpan = $('.personal__ticket__counter__count__item__data__days');
  var hoursSpan = $('.personal__ticket__counter__count__item__data__hours') ;
  var minutesSpan = $('.personal__ticket__counter__count__item__data__min') ;
  
  var time = getTimeRemaining(endtime)

  if(time.seconds === -1){
    return
  }

  function updateClock() {
    var t = getTimeRemaining(endtime);

    daysSpan.html(t.days);
    hoursSpan.html(('0' + t.hours).slice(-2));
    minutesSpan.html(('0' + t.minutes).slice(-2));

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

// var deadline="july 20 2018 11:45:00 GMT+0300";
var deadline= $('.personal__ticket__counter__count').data('time');
initializeClock(deadline);
///////////////////////////////////////////////////////////////////////

$(document).on('click', '.personal__btn__button', function(e){
  e.preventDefault()
  $('.personal-buy-ticket-form-wrap').removeClass('active')
  let blockShow = $(this).data('block');
  $('.personal__btn__button').removeClass('active')
  $(this).addClass('active')
  $('.' + blockShow).addClass('active')
})

/////////////////////////////////////////////////////////////////////////////////////////////////

$('.owl-carousel').owlCarousel({
    loop:true,
    items: 1,
})
$('.owl-carousel').trigger('refresh.owl.carousel')
////////////////////////////////////////////////////////////////////////////////////////////////
  $('#fullpage').fullpage({
    //options here
    scrollHorizontally: true,
    anchors:['1page', '2page'],
    // scrollOverflow: false,
    lazyLoading: true,
    controlArrows: false,
    slidesNavigation: true,
    responsiveWidth: 768,
    responsiveSlides: true,
    scrollBar: false,
    paddingTop: '50px',
    paddingBottom: '50px',
    fixedElements: ' .soc-wrap, .site-footer, .slide-counter',
    afterLoad: function(link, index){
      let navIndex = index.index + 1
      if(navIndex == 1){
        $('.slide-counter-span-num').text(navIndex)
      }
      if(navIndex == 2){
        $('.slide-counter-span-num').text(navIndex)
        
      }
  }

});

})
