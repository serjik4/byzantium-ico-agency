<!-- modal -->
<div class="modal get-invite-modal">
    <button class="close-modal js-close-modal">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
					<g>
                        <polygon points="612,36.004 576.521,0.603 306,270.608 35.478,0.603 0,36.004 270.522,306.011 0,575.997 35.478,611.397 306,341.411 576.521,611.397 612,575.997 341.459,306.011"/>
                    </g>
				</svg>
    </button>
    <div class="decor-cross">
        <img src="{{asset('img/many_cross.png')}}" alt="">
    </div>
    <div class="inner-container get-invite-inner">
        <div class="modal-content">    
            <div class="modal-title">Get an Invite</div>
            <div class="modal-pre-text">Leave your information below and we’ll proceed your request as soon as we can.</div>
            <form id="invite-form" action="" class="modal-form" method="POST" accept-charset="utf-8">
                <div class="modal-form-row get-invite-modal-row">
                    <span class="modal-form-input-title">Name</span>
                    <input name="first_name" type="text" class="modal-form-input" required>
                </div>
                <div class="modal-form-row get-invite-modal-row">
                    <span class="modal-form-input-title">Last Name</span>
                    <input name="last_name" type="text" class="modal-form-input" required>
                </div>
                <div class="modal-form-row get-invite-modal-row">
                    <span class="modal-form-input-title">Email</span>
                    <input name="email" type="email" class="modal-form-input" required>
                </div>
                <div class="modal-form-row get-invite-modal-row">
                    <span class="modal-form-input-title">Company</span>
                    <input name="company" type="text" class="modal-form-input" required>
                </div>
                <div class="modal-form-row get-invite-modal-row">
                    <span class="modal-form-input-title">Title</span>
                    <input name="title" type="text" class="modal-form-input">
                </div>
                <div class="modal-form-row get-invite-modal-row">
                    <span class="modal-form-input-title">Message</span>
                    <input name="message" type="text" class="modal-form-input">
                </div>
                <div class="modal-form-row get-invite-modal-row">
                    <button type="submit" class="btn-green btn-small submit-btn get-invite-request-btn">Send request</button>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
            </form>
        </div>
        <div class="modal-img modal-img-lion glitch">
            {{--<img src="{{asset('public/img/statue_lion.png')}}" alt="Byzantium #Lion">--}}
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal become-sponsor-modal">
    <button class="close-modal js-close-modal">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
					<g>
                        <polygon points="612,36.004 576.521,0.603 306,270.608 35.478,0.603 0,36.004 270.522,306.011 0,575.997 35.478,611.397 306,341.411 576.521,611.397 612,575.997 341.459,306.011"/>
                    </g>
				</svg>
    </button>
    <div class="decor-cross">
        <img src="{{asset('img/many_cross.png')}}" alt="">
    </div>
    <div class="inner-container become-sponsor-inner">
        <div class="modal-content">
            <div class="modal-title">Become a Sponsor</div>
            <div class="modal-pre-text">Leave your information below and we’ll provide you with information about sponsorship packages as soon as we can.</div>
            <form id="sponsor-form" action="" method="POST" class="modal-form" accept-charset="utf-8">
                <div class="modal-form-row become-sponsor-modal-row">
                    <span class="modal-form-input-title">First Name</span>
                    <input name="first_name" type="text" class="modal-form-input" required>
                </div>
                <div class="modal-form-row become-sponsor-modal-row">
                    <span class="modal-form-input-title">Last Name</span>
                    <input name="last_name" type="text" class="modal-form-input" required>
                </div>
                <div class="modal-form-row become-sponsor-modal-row">
                    <span class="modal-form-input-title">Email</span>
                    <input name="email" type="email" class="modal-form-input" required>
                </div>
                <div class="modal-form-row become-sponsor-modal-row">
                    <span class="modal-form-input-title">Company</span>
                    <input name="company" type="text" class="modal-form-input" required>
                </div>
                <div class="modal-form-row become-sponsor-modal-row">
                    <span class="modal-form-input-title">Title</span>
                    <input name="title_t" type="text" class="modal-form-input">
                </div>
                <div class="modal-form-row become-sponsor-modal-row">
                    <span class="modal-form-input-title">Message</span>
                    <input name="message" type="text" class="modal-form-input">
                </div>
                <div class="modal-form-row become-sponsor-modal-row">
                    <button type="submit" class="btn-green btn-small submit-btn become-sponsor-request-btn">Send request</button>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
            </form>
        </div>
        <div class="modal-img modal-img-lion glitch">
            {{--<img src="{{asset('img/statue_lion.png')}}" alt="Byzantium #Lion">--}}
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal contact-modal">
    <button class="close-modal js-close-modal">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
					<g>
                        <polygon points="612,36.004 576.521,0.603 306,270.608 35.478,0.603 0,36.004 270.522,306.011 0,575.997 35.478,611.397 306,341.411 576.521,611.397 612,575.997 341.459,306.011"/>
                    </g>
				</svg>
    </button>
    <div class="decor-cross">
        <img src="{{asset('img/many_cross.png')}}" alt="">
    </div>
    <div class="inner-container contact-inner">
        <div class="modal-content">
            <div class="modal-title">Contact</div>
            <div class="modal-pre-text">Got a question? We’d love to hear from you. Send us a message and we’ll respond as soon as possible.</div>
            <form action="" class="modal-form modal-form-contact" id="contact-form" method="POST" accept-charset="utf-8">
                <div class="modal-form-row contact-modal-row">
                    <span class="modal-form-input-title">Name</span>
                    <input name="name" type="text" class="modal-form-input" required>
                </div>
                <div class="modal-form-row contact-modal-row">
                    <span class="modal-form-input-title">Email</span>
                    <input name="email" type="email" class="modal-form-input" required>
                </div>
                <div class="modal-form-row contact-modal-row">
                    <span class="modal-form-input-title">Message</span>
                    <input name="message" type="text" class="modal-form-input" required>
                </div>
                <div class="modal-form-row contact-modal-row">
                    <button type="submit" class="btn-green btn-small submit-btn">Send message</button>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
            </form>
        </div>
        <div class="modal-img modal-img-lion glitch">
            {{--<img src="{{asset('img/statue_lion.png')}}" alt="Byzantium #Lion">--}}
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal thank-you-modal">
    <button class="close-modal js-close-modal">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
            <g>
                <polygon points="612,36.004 576.521,0.603 306,270.608 35.478,0.603 0,36.004 270.522,306.011 0,575.997 35.478,611.397 306,341.411 576.521,611.397 612,575.997 341.459,306.011"/>
            </g>
        </svg>
    </button>
    <div class="inner-container thank-you-modal-inner">
        <div class="modal-content">
            <div class="modal-title">Thanks, we’ll be in touch shortly</div>
            <div class="modal-pre-text">Someone on our team is reviewing your request and will contact you as soon as possible.</div>
        </div>
        <div class="modal-img modal-statue-thank-you-img glitch">
            <img src="{{asset('img/statue_thank-you.png')}}" alt="Byzantium #Thanks" class="modal-statue-thank-you-img">
        </div>
    </div>
</div>