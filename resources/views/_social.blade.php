<div class="soc-wrap">
    <a href="#" class="soc-wrap-link js-contact-us"><i class="fa fa-envelope"></i></a>
    <a target="_blank" href="https://t.me/top10pointoftrust" class="soc-wrap-link"><i class="fa fa-telegram"></i></a>
    <a target="_blank" href="https://twitter.com/top10_of_trust" class="soc-wrap-link"><i class="fa fa-twitter"></i></a>
    <a target="_blank" href="https://facebook.com/top10pointoftrust" class="soc-wrap-link"><i class="fa fa-facebook"></i></a>
</div>