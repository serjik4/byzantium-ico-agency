@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавить событие
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'post', 'route' => 'speakers.store', 'files' => 'true'])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем спикера</h3>
                    @include('admin.error')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Название</label>
                            <input type="text" name="title" value="{{old('title')}}" class="form-control" id="exampleInputName1" placeholder="Название">
                        </div>
                        {{--<div class="form-group">
                            <label for="exampleInputDescription1">Биография</label>
                            <textarea name="description" id="" cols="30" rows="7" class="form-control" id="exampleInputDescription1" placeholder="Биография">{{old('description')}}</textarea>
                        </div>--}}
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                <label>Начало:</label>

                                <div class="input-group date datetimepicker2">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="time_start" class="form-control pull-right">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label>Окончание:</label>

                                <div class="input-group date datetimepicker2">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="time_end" class="form-control pull-right">
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                    </div>
                </div>
                {{--hide input--}}
                <input type="hidden" name="is_speaker" value="0">
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('speakers.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection