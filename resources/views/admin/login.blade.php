@extends('layout')

@section('main')
    {{Form::open(['method' => 'post', 'route' => 'bzntm.login', 'class' => 'auth-form', 'style' => 'margin: 0 auto;'])}}
        <div class="form-title">Log <span class="title-decor-cross">in</span></div>
        <div class="form-input-row">
            <span class="form-input-title">Email</span>
            <input type="text" class="form-input" name="email" value="{{old('email')}}">
        </div>
        <div class="form-input-row">
            <span class="form-input-title">Password</span>
            <input type="password" class="form-input" name="password">
        </div>
        <div class="form-submit">
            <button type="submit" class="btn-green btn-medium">Login</button>
        </div>
    {{Form::close()}}
@endsection