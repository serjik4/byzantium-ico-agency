@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Редактирование участника
            </h1>
            @if(session('status'))
                <div style="color: #ff000e;">{!! session('status') !!}</div>
            @endif
        </section>

    <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'put', 'route' => ['participants.update', $participant->id], 'files' => 'true'])}}
        <!-- Default box -->
            <div class="form-group" style="text-align: right">
                <label>
                    <input name="is_pay" onchange="return alert('Вы изменили статус Оплачено')" type="checkbox" class="" @if($participant->is_pay){{'checked'}}@endif>
                </label>
                <label>
                    Оплачено
                </label>
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование участника</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Имя</label>
                            <input type="text" name="first_name" value="{{$participant->first_name}}" class="form-control" id="exampleInputName1" placeholder="Имя">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputLastLastName1">Фамилия</label>
                            <input name="last_name" type="text" value="{{$participant->last_name}}" class="form-control" id="exampleInputLastName1" placeholder="Фамилия">
                        </div>

                        {{--Date--}}
                        <div class="form-group">
                            <label>Дата рождения:</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="birthday" type="text" class="form-control pull-right" id="datepicker" placeholder="Дата рождения" value="{{$participant->birthday}}">
                            </div>
                            <!-- /.input group -->
                        </div>

                        <div class="form-group">
                            <label for="exampleInputCompany1">Компания</label>
                            <input type="text" name="company" value="{{$participant->company}}" class="form-control" id="exampleInputCompany1" placeholder="Компания">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPosition1">Должность</label>
                            <input type="text" name="position" value="{{$participant->position}}" class="form-control" id="exampleInputPosition1" placeholder="Должность">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputCountry1">Страна</label>
                            <input type="text" name="country" value="{{$participant->country}}" class="form-control" id="exampleInputCountry1" placeholder="Страна">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputCity1">Город</label>
                            <input type="text" name="city" value="{{$participant->city}}" class="form-control" id="exampleInputCity1" placeholder="Город">
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <img src="{{asset($participant->getPhoto())}}" alt="pic" class="img-responsive" width="200">
                            <label for="exampleInputFile">Аватар</label>
                            <input type="file" name="photo" id="exampleInputFile">

                            <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputName1">Email</label>
                            <input type="text" name="email" value="{{$participant->email}}" class="form-control disabled" id="exampleInputName1" disabled="disabled">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputLastLastName1">Промокод</label>
                            <input name="promo_code" type="text" value="{{$participant->getParticipantPromoCode()}}" class="form-control disabled" id="exampleInputLastName1">
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('participants.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection