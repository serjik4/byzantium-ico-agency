@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Участники
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Участники</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        {{--<a href="{{route('participants.create')}}" class="btn btn-success">Добавить</a>--}}
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Имя</th>
                            <th>E-mail</th>
                            <th>Аватар</th>
                            <th>Промокод</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($participants as $participant)
                            <tr>
                                <td>{{$participant->id}}</td>
                                <td>{{$participant->first_name}}</td>
                                <td>{{$participant->email}}</td>
                                <td>
                                    <img src="{{asset($participant->getPhoto())}}" alt="" class="img-responsive" width="150">
                                </td>
                                <td>{{$participant->getParticipantPromoCode()}}</td>
                                <td>
                                    <a href="{{route('participants.edit', [$participant->id])}}" class="fa fa-pencil"></a>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection