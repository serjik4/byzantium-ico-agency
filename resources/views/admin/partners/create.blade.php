@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавить партнера
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            {{Form::open(['method' => 'post', 'route' => 'partners.store', 'files' => 'true'])}}
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем партнера</h3>
                    @include('admin.error')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Название</label>
                            <input type="text" name="title" value="{{old('title')}}" class="form-control" id="exampleInputName1" placeholder="Название">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputDescription1">Описание</label>
                            <textarea name="description" id="" cols="30" rows="7" class="form-control" id="exampleInputDescription1" placeholder="Описание">{{old('description')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Аватар</label>
                            <input type="file" name="photo" id="exampleInputFile">

                            <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFB1">Facebook</label>
                            <input type="text" name="facebook_link" class="form-control" id="exampleInputFB1" placeholder="Facebook">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputLIn1">LinkedIn</label>
                            <input type="text" name="linkedin_link" class="form-control" id="exampleInputLIn1" placeholder="LinkedIn">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('partners.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection