@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Партнеры
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Партнеры</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <a href="{{route('partners.create')}}" class="btn btn-success">Добавить</a>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Аватар</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($partners as $partner)
                            <tr>
                                <td>{{$partner->id}}</td>
                                <td>{{$partner->title}}</td>
                                <td>{{$partner->description}}</td>
                                <td>
                                    <img src="{{asset($partner->getPhoto())}}" alt="" class="img-responsive" width="150">
                                </td>
                                <td>
                                    <a href="{{route('partners.edit', $partner->id)}}" class="fa fa-pencil"></a>
                                    {{Form::open(['method' => 'delete', 'route' => ['partners.destroy', $partner->id]])}}
                                        <button type="submit" class="delete" onclick="return confirm('Вы уверены, что хотите удалить партнера?')">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection