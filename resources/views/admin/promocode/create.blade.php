@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавить промокод
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            {{Form::open(['method' => 'post', 'route' => 'codes.store'])}}
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Добавляем промокод</h3>
                        @include('admin.error')
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPromo1">Промокод</label>
                                <input type="text" name="code" class="form-control" id="exampleInputPromo1" placeholder="Промокод">
                            </div>
                            <!-- Date -->
                            <div class="form-group">
                                <label>Действителен до:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="expirationDate" class="form-control pull-right" id="datepicker">
                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <label>Source:</label>
                                <select name="source" class="form-control select2" data-placeholder="Выберите" style="width: 100%;">
                                    <option value="Conference">Conference</option>
                                    <option value="Referrals">Referrals</option>
                                    <option value="Free">Free</option>
                                    <option value="Media">Media</option>
                                    <option value="Waitlist">Waitlist</option>
                                    <option value="Influencers">Influencers</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{route('codes.index')}}" class="btn btn-default">Назад</a>
                        <button type="submit" class="btn btn-success pull-right">Добавить</button>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection