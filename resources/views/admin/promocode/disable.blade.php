@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Деактивированные промокоды
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Деактивированные промокоды</h3>
                    @include('admin.error')
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Source</th>
                            <th>Промокод</th>
                            <th>Email</th>
                            <th>Статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($promocodes as $promocode)
                            <tr>
                                <td>{{$promocode->id}}</td>
                                <td>{{$promocode->source}}</td>
                                <td>{{$promocode->code}}</td>
                                <td>{{$promocode->getParticipantEmail()}}</td>
                                <td>
                                    @if($promocode->status == 1)
                                        {{--<i class="fa fa-thumbs-o-up"></i>--}}
                                        Активен
                                    @elseif($promocode->status == -1)
                                        {{--<i class="fa fa-lock"></i>--}}
                                        Отключен
                                    @else
                                        Зарезервирован
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection