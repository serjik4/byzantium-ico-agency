@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="display: flex;justify-content: space-between;">
            <h1>
                Промокод
            </h1>
            {{Form::open(['method' => 'post', 'url' => 'panel/codes/xls'])}}
            <button type="submit" class="btn btn-success pull-right">Выгрузить промокоды</button>
            {{Form::close()}}
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Промокод</h3>
                    @include('admin.error')
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group" style="display: flex;justify-content: space-between;align-items: center;">
                        <a href="{{route('codes.create')}}" class="btn btn-success">Добавить один</a>
                        {{Form::open(['method' => 'post', 'route' => 'generate.codes', 'style' => 'display:flex;align-items:center;', 'id' => 'generate'])}}
                            <div class="form-group">
                                <label>Действителен до:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="expirationDate" type="text" class="form-control pull-right" id="datepicker" placeholder="Действителен до">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group">
                                <label>Source:</label>
                                <select name="source" class="form-control select2" id="source" data-placeholder="Выберите" style="width: 100%;">
                                    <option value="Conference">Conference</option>
                                    <option value="Referrals">Referrals</option>
                                    <option value="Free">Free</option>
                                    <option value="Media">Media</option>
                                    <option value="Waitlist">Waitlist</option>
                                    <option value="Influencers">Influencers</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPromo1">Промокод</label>
                                <input type="text" name="count" class="form-control" id="exampleInputPromo1" placeholder="Количество" value="{{old('count')}}">
                            </div>
                            <button type="submit" class="btn btn-success pull-right">Генерировать</button>
                        {{Form::close()}}
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Source</th>
                            <th>Промокод</th>
                            <th>Email</th>
                            <th>Действителен до</th>
                            <th>Статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($promocodes as $promocode)
                            <tr>
                                <td>{{$promocode->id}}</td>
                                <td>{{$promocode->source}}</td>
                                <td>{{$promocode->code}}</td>
                                <td>
                                    @if($promocode->participant)
                                        <a href="{{route('participants.edit', $promocode->getParticipantId())}}">
                                            {{$promocode->getParticipantEmail()}}
                                        </a>
                                    @else
                                        {{$promocode->getParticipantEmail()}}
                                    @endif
                                </td>
                                <td>{{$promocode->expirationDate}}</td>
                                <td>
                                    @if($promocode->status == 1)
                                        {{--<i class="fa fa-thumbs-o-up"></i>--}}
                                        Активен
                                    @elseif($promocode->status == -1)
                                        {{--<i class="fa fa-lock"></i>--}}
                                        Отключен
                                    @elseif($promocode->isPay())
                                        Оплачен
                                    @else
                                        Зарезервирован
                                    @endif
                                    {{Form::open(['method' => 'delete', 'route' => ['codes.destroy', $promocode->id]])}}
                                    <button type="submit" class="delete" onclick="return confirm('Вы уверены, что хотите удалить промокод?')">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection