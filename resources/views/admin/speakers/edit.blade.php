@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Редактирование спикера
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'put', 'route' => ['speakers.update', $speaker->id], 'files' => 'true'])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование спикера</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Имя</label>
                            <input type="text" name="title" value="{{$speaker->title}}" class="form-control" id="exampleInputName1" placeholder="Имя">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputDescription1">Биография</label>
                            <textarea name="description" id="" cols="30" rows="7" class="form-control" id="exampleInputDescription1" placeholder="Биография">{{$speaker->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputCompany1">Должность</label>
                            <input type="text" name="company" value="{{$speaker->company}}" class="form-control" id="exampleInputCompany1" placeholder="Должность">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFB1">Facebook</label>
                            <input type="text" name="facebook_link" value="{{$speaker->facebook_link}}" class="form-control" id="exampleInputFB1" placeholder="Facebook">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputLIn1">LinkedIn</label>
                            <input type="text" name="linkedin_link" value="{{$speaker->linkedin_link}}" class="form-control" id="exampleInputLIn1" placeholder="LinkedIn">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <img src="{{asset($speaker->getPhoto())}}" alt="pic" class="img-responsive" width="200">
                            <label for="exampleInputFile">Аватар</label>
                            <input type="file" name="photo" id="exampleInputFile">

                            <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                <label>Начало выступления:</label>

                                <div class="input-group date datetimepicker2">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="time_start" class="form-control pull-right" value="{{$speaker->time_start}}">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label>Окончание выступления:</label>

                                <div class="input-group date datetimepicker2">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="time_end" class="form-control pull-right" value="{{$speaker->time_end}}">
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                    </div>
                </div>
                {{--hide input is_speaker--}}
                <input type="hidden" name="is_speaker" value="1">
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('speakers.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection