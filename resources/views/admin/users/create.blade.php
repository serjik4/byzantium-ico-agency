@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавить пользователя
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'post', 'route' => 'users.store', 'file' => true])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем пользователя</h3>
                    @include('admin.error')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputLogi1">Логин</label>
                            <input type="text" name="username" value="{{old('username')}}" class="form-control" id="exampleInputLogin1" placeholder="Логин">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">E-mail</label>
                            <input type="email" name="email" value="{{old('email')}}" class="form-control" id="exampleInputEmail1" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Пароль</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Пароль">
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label for="exampleInputFile">Аватар</label>--}}
                            {{--<input type="file" id="exampleInputFile">--}}

                            {{--<p class="help-block">Какое-нибудь уведомление о форматах..</p>--}}
                        {{--</div>--}}
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('users.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection