@extends('cabinet.layout')

@section('main')
    <main class="main">
        <div class="decor-wave-two-personal">
            <img src="{{asset('public/img/svg/two_white_wave.svg')}}" alt="">
        </div>
        <div class="decor-wave-two-personal-down">
            <img src="{{asset('public/img/svg/two_white_wave.svg')}}" alt="">
        </div>
        @if(!$participant->is_pay)
            <div class="container">
                <div class="personal__ticket">
                    <div class="personal__ticket__price">
                        <div class="personal__ticket__price__title">
                            Ticket Price
                        </div>
                        <div class="personal__ticket__price__payment">
                            <span>690$</span> <span class="personal__ticket__price__payment__span">990$</span>
                        </div>
                    </div>
                    <div class="personal__ticket__counter">
                        <div class="personal__ticket__counter__title">
                            Your code expires in
                        </div>
                        <div class="personal__ticket__counter__count" data-time="{{$participant->getExpirationDate()}}">
                            <div class="personal__ticket__counter__count__item">
                                <span class="personal__ticket__counter__count__item__data__days">20</span> <span class="personal__ticket__counter__count__item__span">days</span>
                            </div>
                            <div class="personal__ticket__counter__count__item">
                                <span class="personal__ticket__counter__count__item__data__hours">20</span> <span class="personal__ticket__counter__count__item__span">
                                hours
								</span>
                            </div>
                            <div class="personal__ticket__counter__count__item">
                                <span class="personal__ticket__counter__count__item__data__min">20</span> <span class="personal__ticket__counter__count__item__span">mins</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__btn">
                    <button  class="personal__btn__button active" data-block="personal-buy-ticket-form-wrap-money">
                        Pay with Card
                    </button>
                    <button  class="personal__btn__button " data-block="personal-buy-ticket-form-wrap-crypto">Pay with Crypto</button>
                    <div class="personal__btn__cross__img">
                        <img src="{{asset('public/img/svg/green_cross.svg')}}" alt="">
                        <img src="{{asset('public/img/svg/white_cross.svg')}}" alt="">
                    </div>
                </div>
                <div class="personal-buy-ticket-form-wrap-money personal-buy-ticket-form-wrap active" data-block="personal-bay-ticket-form-wrap-money">
                    <!-- Ticket Tailor Widget. Paste this in to your website where you want the widget to appear. Do no change the code or the widget may not work properly. -->
                    <div class="tt-widget"><div class="tt-widget-fallback"><p><a href="https://www.tickettailor.com/new-order/179411/e3ec/ref/website_widget/" target="_blank">Click here to buy tickets</a><br /><small><a href="http://www.tickettailor.com?rf=wdg" class="tt-widget-powered">Sell tickets online with Ticket Tailor</a></small></p></div><script src="https://dc161a0a89fedd6639c9-03787a0970cd749432e2a6d3b34c55df.ssl.cf3.rackcdn.com/tt-widget.js" data-url="https://www.tickettailor.com/new-order/179411/e3ec/ref/website_widget/" data-type="inline" data-inline-minimal="false" data-inline-show-logo="true" data-inline-bg-fill="true"></script></div>
                    <!-- End of Ticket Tailor Widget -->
                </div>
                <div class="personal-buy-ticket-form-wrap-crypto personal-buy-ticket-form-wrap" data-block="personal-bay-ticket-form-wrap-crypto">
                    <p class="personal-buy-ticket-attention">Coming soon</p>
                    {{--<form action="https://www.coinpayments.net/index.php" method="post">--}}
                    {{--<input type="hidden" name="cmd" value="_pay_simple">--}}
                    {{--<input type="hidden" name="reset" value="1">--}}
                    {{--<input type="hidden" name="merchant" value="606a89bb575311badf510a4a8b79a45e">--}}
                    {{--<input type="hidden" name="currency" value="LTC">--}}
                    {{--<input type="hidden" name="amountf" value="10.00">--}}
                    {{--<input type="hidden" name="item_name" value="Test Item">--}}
                    {{--<input class="coinpayment-form-input-img" type="image" src="https://www.coinpayments.net/images/pub/buynow-grey.png" alt="Купить используя CoinPayments.net">--}}
                    {{--</form>--}}
                </div>
            </div>
        @else
            <div>
                <p class="thank-you-text">Thank you!</p>
                <p class="thank-you-text">Your ticket has been sent to your email.</p>
            </div>
        @endif
    </main>
@endsection