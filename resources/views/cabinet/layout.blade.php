<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Byzantium - Sign In</title>
    <link rel="shortcut icon" href="{{asset('public/img/favicon/favicon.ico')}}" type="image/x-icon">
    {{--<link rel="stylesheet" href="libs/font-awesome/css/font-awesome.min.css">--}}
    {{--<link rel="stylesheet" href="css/libs.min.css">--}}
    <link rel="stylesheet" href="{{asset('public/css/front.css')}}">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PCXP5MW');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PCXP5MW"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="wrapper">
    <header class="header">
        <div class="header-wrap">
            <a href="{{url('/')}}" class="header-logo">
                <img src="{{asset('public/img/logo-company.png')}}" alt="logo-company.png">
            </a>
            <div class="header-contact">
                <span class="header-contact-right">
                    {{$participant->email}}
                </span>
                <span class="header-contact-left">
                    {{$participant->promocode->code}}
                </span>
                <a href="{{url('/cabinet/logout')}}" class="header-logout" title="logout">
                    <img src="{{asset('img/svg/exit.svg')}}" alt="logout">
                </a>
            </div>
        </div>
        {{--<div class="back-link-wrap">
            <div class="back-link-inner">
                <a href="" class="back-link">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 240.823 240.823" style="enable-background:new 0 0 240.823 240.823;" xml:space="preserve">
                        <path d="M57.633,129.007L165.93,237.268c4.752,4.74,12.451,4.74,17.215,0c4.752-4.74,4.752-12.439,0-17.179
                        l-99.707-99.671l99.695-99.671c4.752-4.74,4.752-12.439,0-17.191c-4.752-4.74-12.463-4.74-17.215,0L57.621,111.816
                        C52.942,116.507,52.942,124.327,57.633,129.007z"/>
                    </svg>
                    <span>Back</span>
                </a>
            </div>
        </div>--}}
    </header>
        @yield('main')
    <!-- <script src="libs/jquery/dist/jquery.min.js"></script> -->
    <script src="{{asset('public/js/front.js')}}"></script>
    {{--<script src="js/common.js"></script>--}}
</body>
</html>