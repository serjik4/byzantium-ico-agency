Welcome to Top10 Point of Trust!<br />
<br />
Please, use the following login details to access your account and buy your ticket.<br />
<br />
Login: {{$email}}<br />
Your personal promocode: {{$password}}<br />
<br />
If you have any questions, don’t hesitate to contact us at top10@bzntm.com.<br />
<br />
Best,<br />
Top10 Attendee Team<br />