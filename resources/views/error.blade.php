@if($errors->any())
    @foreach($errors->all() as $error)
        <div style="color: #ff5959;text-align: center;padding: 10px 0;">{{$error}}</div>
    @endforeach
@endif