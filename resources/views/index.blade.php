@extends('layout')

@section('main')

<div id="fullpage">
    <div class="main section">
        <div class="container">
            <div class="main-content">
                <div class="img-wrap-statue glitch">
                    <!-- <img src="img/statue.png" alt="Statue.png"> -->
                </div>
                <div class="right-wrap">
                    <!-- <h3>11th of October</h3> -->
                    <!-- <div class="text-up-wrap">
                    <span class="green-text">Top10 </span></div>
                    <h2>Point of Trust</h2>
                    <p class="blue-txt">Casa Llotja de Mar, Barcelona, Spain</p>
                    <p>First-of-a-kind event with ten biggest successes<span> of the blockchain world under one roof</span></p> -->

                    <h3>11th of October</h3>
                    <p class="blue-txt">Casa Llotja de Mar, Barcelona, Spain</p>
                    <h2>Top10 Point of Trust</h2>
                    <p>First-of-a-kind event with ten biggest successes<span> of the blockchain world under one roof</span></p>

                    <div class="price">
                        <div class="price-title">
                            Early Bird Price
                        </div>
                        <div class="price-count">
                            $690 <span class="price-count-line">$990</span>
                        </div>
                    </div>
                    @if(!Auth::guard('participant')->check())
                        <button class="btn-green btn-medium js-get-invite-link">Get an invite</button>
                        <button class="become-sponsor-link js-become-sponsor-link mobile">Become a sponsor</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="main section media">
        <div class="container media-container">
            <div class="main-content media-content">
                <div class="media-title"><span>Media</span> partners</div>
            {{--    <div class="media-slider desc">
                    <div class="owl-carousel owl-wrap">
                        <div class="item slide-item">
                            <img src="#" data-src="{{asset('img/svg/partners/bitcoincom.svg')}}" alt="logo-company">
                            <img src="#" data-src="{{asset('img/svg/partners/bitcoingarden.svg')}}" alt="logo-company">
                            <img src="#" data-src="{{asset('img/svg/partners/bitcoinist.svg')}}" alt="logo-company">
                            <img src="#" data-src="{{asset('img/svg/partners/bitcoinmarketjournal.svg')}}" alt="logo-company">
                        </div>
                        <div class="item slide-item">
                            <img src="#" data-src="{{asset('img/svg/partners/btcmanager.svg')}}" alt="logo-company">
                            <img src="#" data-src="{{asset('img/svg/partners/coinspeaker.svg')}}" alt="logo-company">
                            <img src="#" data-src="{{asset('img/svg/partners/newsbtc.svg')}}" alt="logo-company">
                        </div>
                    </div>
                </div> --}} 

                <div class="media-slider mob">
                    <div class="owl-carousel owl-wrap">
                        <div class="item slide-item">
                            <a target="_blank" href="http://bitcoin.com/"><img src="#" data-src="{{asset('img/svg/partners/bitcoincom.svg')}}" alt="logo-company"></a>
                        </div>
                        <div class="item slide-item">
                            <a target="_blank" href="http://bitcoingarden.org/"><img src="#" data-src="{{asset('img/svg/partners/bitcoingarden.svg')}}" alt="logo-company"></a>

                        </div>
                        <div class="item slide-item">
                            <a target="_blank" href="http://bitcoinist.com/"><img src="#" data-src="{{asset('img/svg/partners/bitcoinist.svg')}}" alt="logo-company"></a>

                        </div>
                        <div class="item slide-item">
                            <a target="_blank" href="https://www.bitcoinmarketjournal.com"><img src="#" data-src="{{asset('img/svg/partners/bitcoinmarketjournal.svg')}}" alt="logo-company"></a>

                        </div>
                        <div class="item slide-item">
                            <a target="_blank" href="http://btcmanager.com/"><img src="#" data-src="{{asset('img/svg/partners/btcmanager.svg')}}" alt="logo-company"></a>
                            
                        </div>
                        <div class="item slide-item">
                            <a target="_blank" href="https://www.coinspeaker.com/"><img src="#" data-src="{{asset('img/svg/partners/coinspeaker.svg')}}" alt="logo-company"></a>
                        </div>
                        <div class="item slide-item">
                            <a target="_blank" href="http://newsbtc.com/"><img src="#" data-src="{{asset('img/svg/partners/newsbtc.svg')}}" alt="logo-company"></a>
                        </div>
                    </div>
                </div>


                <div class="media-slider desc">
                    <div class="media-logo">
                        <a target="_blank" href="http://bitcoin.com/"><img src="#" data-src="{{asset('img/svg/partners/bitcoincom.svg')}}" alt="logo-company"></a>
                        <a target="_blank" href="http://bitcoingarden.org/"><img src="#" data-src="{{asset('img/svg/partners/bitcoingarden.svg')}}" alt="logo-company"></a>
                        <a target="_blank" href="http://bitcoinist.com/"><img src="#" data-src="{{asset('img/svg/partners/bitcoinist.svg')}}" alt="logo-company"></a>
                        <a target="_blank" href="https://www.bitcoinmarketjournal.com"><img src="#" data-src="{{asset('img/svg/partners/bitcoinmarketjournal.svg')}}" alt="logo-company"></a>
                    </div>
                    <div class="media-logo after ">
                        <a target="_blank" href="http://btcmanager.com/"><img src="#" data-src="{{asset('img/svg/partners/btcmanager.svg')}}" alt="logo-company"></a>
                        <a target="_blank" href="https://www.coinspeaker.com/"><img src="#" data-src="{{asset('img/svg/partners/coinspeaker.svg')}}" alt="logo-company"></a>
                        <a target="_blank" href="http://newsbtc.com/"><img src="#" data-src="{{asset('img/svg/partners/newsbtc.svg')}}" alt="logo-company"></a>
                    </div>
                </div>

            </div>

        </div>
        @include('_social')
        @include('_slidecount')
    </div>
    @endsection