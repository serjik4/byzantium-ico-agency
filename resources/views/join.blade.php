<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Byzantium - Sign In</title>
    <link rel="shortcut icon" href="{{asset('public/img/favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('public/css/front.css')}}">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PCXP5MW');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PCXP5MW"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="wrapper">
    <header class="header">
        <div class="header-wrap">
            <a href="{{url('/')}}" class="header-logo">
                <img src="{{asset('public/img/logo-company.png')}}" alt="logo-company.png">
            </a>
        </div>

        <div class="back-link-wrap">
            <div class="back-link-inner">
                <a href="{{url()->previous()}}" class="back-link">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 240.823 240.823" style="enable-background:new 0 0 240.823 240.823;" xml:space="preserve">
                            <path d="M57.633,129.007L165.93,237.268c4.752,4.74,12.451,4.74,17.215,0c4.752-4.74,4.752-12.439,0-17.179
                                l-99.707-99.671l99.695-99.671c4.752-4.74,4.752-12.439,0-17.191c-4.752-4.74-12.463-4.74-17.215,0L57.621,111.816
                                C52.942,116.507,52.942,124.327,57.633,129.007z"/>
                        </svg>
                    <span>Back</span>
                </a>
            </div>
        </div>

    </header>
    <main class="main">
        <div class="decor-cross">
            <img src="{{asset('public/img/svg/many_green_cross.svg')}}" alt="">
        </div>

        <div class="container">
            <div class="main-content auth-page">
                <form action="{{route('register')}}" method="post" class="auth-form">
                    {{csrf_field()}}
                    <div class="decor-wave-one">
                        <img src="{{asset('public/img/svg/white_wave.svg')}}" alt="">
                    </div>
                    <div class="decor-wave-two">
                        <img src="{{asset('public/img/svg/two_white_wave.svg')}}" alt="">
                    </div>
                    <div class="form-title">Use my <span class="title-decor-cross">Code</span></div>
                    <div class="form-desc auth-form-desc">Paste your code to create your account</div>
                    @include('error')
                    @if(session('status'))
                        <div style="color: #ff5959;text-align: center;padding: 10px 0;">{!!session('status')!!}</div>
                    @endif
                    <div class="form-input-row">
                        <span class="form-input-title">E-mail</span>
                        <input name="email" type="text" class="form-input" value="{{old('email')}}" required>
                    </div>
                    <div class="form-input-row">
                        <span class="form-input-title">Promocode</span>
                        <input name="code" type="text" class="form-input" required>
                    </div>
                    <div class="form-already-acc">Already have an account? <a href="{{url('/login')}}">Log in</a></div>
                    <div class="form-submit">
                        <button class="btn-green btn-medium">Sign up</button>
                    </div>
                </form>
                <div class="above">
                    <span>Click "Sign up" above to accept Top10</span>
                    <p><a href="{{url('/terms-of-use')}}">Terms of Use</a> and <a href="{{url('/privacy-policy')}}">Privacy Policy</a></p>
                </div>
            </div>
        </div>
        @include('_social')
    </main>
</div>

@include('_modal')

<script src="{{asset('public/js/front.js')}}"></script>
</body>
</html>