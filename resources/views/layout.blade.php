<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- <base href="/"> -->

    <title>TOP10 Point of Trust</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="TOP10 Point of Trust" />

    <!--     <meta property="og:title" content="TOP10 Point of Trust" />
        <meta property="og:description" content="TOP10 Point of Trust" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://top10.bzntm.com" />
        <meta property="og:image" content="/img/cover.png" />

        <meta property="twitter:card" content="summary" />
        <meta property="twitter:title" content="TOP10 Point of Trust" />
        <meta property="twitter:description" content="TOP10 Point of Trust" />
        <meta property="twitter:image" content="/img/cover.png" /> -->

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">

    <link rel="icon" href="img/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('img/favicon/manifest.json')}}">
    
    
    <link rel="stylesheet" href="{{asset('public/css/front.css')}}">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PCXP5MW');</script>
    <!-- End Google Tag Manager -->
</head>
<body class="main-page">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PCXP5MW"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="wrapper">
    <header class="header">
        <!-- <div class="container"> -->
        <div class="header-wrap">
            <a href="{{url('/')}}" class="header-logo">
                <img src="{{asset('img/logo-company.png')}}" alt="logo-company.png">
            </a>
            <div class="nav-text">
                <button class="become-sponsor-link js-become-sponsor-link">Become a sponsor</button>
                @if(Auth::guard('participant')->check())
                    <a href="{{url('/cabinet/info')}}" class="use-my-code-link btn-green btn-small">Personal area</a>
                @elseif(Auth::check())
                    <a href="{{url('/panel/participants')}}" class="use-my-code-link btn-green btn-small">Admin panel</a>
                @else
                    <a href="{{url('/join')}}" class="use-my-code-link btn-green btn-small">Use my code</a>
                @endif
            </div>
        </div>
        <!-- </div> -->
    </header>

    @if(session('status'))
        <div style="color: #ffffff;">{{session('status')}}</div>
    @endif
    @include('error')
    @yield('main')

    <footer class="site-footer">
        <div class="footer-wrap">
            <div class="nav-text">
                <a href="{{url('/terms-of-use')}}">Terms of use</a>
                <a href="{{url('/privacy-policy')}}">Privacy policy</a>
            </div>
        </div>
    </footer>
</div>

@include('_modal')

<script src="{{asset('public/js/front.js')}}"></script>
</body>
</html>
