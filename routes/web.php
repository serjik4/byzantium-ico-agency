<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Route::get('/panel', 'ParticipantAuthController@getLogin')->name('login');

Route::group(['prefix' => 'panel', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
    Route::resource('/users', 'UserController')->except(['show']);
    Route::resource('/codes', 'PromoCodeController')->except(['show', 'edit', 'update']);
    Route::post('/codes/xls', 'PromoCodeController@xlsPromoCode');
    Route::get('/codes/disable', 'PromoCodeController@getDisabledPromoCodes')->name('disableCodes.index');
    Route::post('/codes/generate', 'PromoCodeController@generatePromoCodes')->name('generate.codes');
    Route::resource('/partners', 'PartnerController')->except(['show']);
    Route::resource('/speakers', 'SpeakerController')->except(['show']);
    Route::get('/events', 'SpeakerController@indexEvents')->name('events.index');
    Route::get('/events/create', 'SpeakerController@createEvent')->name('events.create');
    Route::get('/events/{id}/edit', 'SpeakerController@editEvent')->name('events.edit');
    Route::resource('/participants', 'ParticipantController')->except(['show', 'create', 'store']);
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('panel/logout', 'Admin\AuthController@logout');
});

Route::group(['middleware' => 'auth:participant'], function () {
    Route::get('cabinet/logout', 'ParticipantAuthController@logout');
});

Route::group(['middleware' => ['guest', 'guest:participant']], function () {
    //login for admins
    Route::get('/bzntm-panel', 'Admin\AuthController@getLogin');
    Route::post('/bzntm-panel', 'Admin\AuthController@login')->name('bzntm.login');
});

Route::group(['middleware' => ['guest:participant', 'guest']], function () {
    //login for participants
    Route::get('/login', 'ParticipantAuthController@getLogin');
    Route::post('/login', 'ParticipantAuthController@login')->name('login');
    Route::get('/join', 'ParticipantAuthController@getRegister');
    Route::post('/join', 'ParticipantAuthController@register')->name('register')->middleware('checkPromoCode');
});

Route::group(['prefix' => 'cabinet', 'namespace' => 'Cabinet', 'middleware' => 'cabinet'], function (){
    Route::get('/info', 'InfoController@index')->name('info.index');
    Route::post('/info', 'InfoController@update')->name('info.update');
    Route::post('/info/update', 'InfoController@updatePassword');
});

Route::post('/invite', 'ParticipantAuthController@invite')->name('invite');

Route::post('/become-sponsor', 'ParticipantAuthController@becomeSponsor')->name('become.sponsor');

Route::post('/contact', 'ParticipantAuthController@contactUs')->name('contact');

//Route::post('/subscribe', 'ParticipantAuthController@subscribe')->name('subscribe');//add participants to /storage/app/list.csv

Route::get('/terms-of-use', function (){
    return view('terms-of-use');//terms-of-use page
});

Route::get('/privacy-policy', function (){
    return view('privacy-policy');//privacy-policy page
});

//Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->middleware('guest:participant', 'checkParticipantPassword')->name('password.email');
//Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->middleware('guest:participant')->name('password.request');
//Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->middleware('guest:participant');
//Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->middleware('guest:participant')->name('password.reset');
